package com.crudspringvue.springvue.Controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.crudspringvue.springvue.Model.Usuario;
import com.crudspringvue.springvue.Repository.UsuarioRepository;

import java.util.List;

@CrossOrigin
@RestController
public class UsuarioController {

    @Autowired
    private UsuarioRepository repositorio;

    @PostMapping("/save")
    public ResponseEntity saveUser(@RequestBody Usuario users){
        if(users != null){
        	System.out.print("Tesasghlh  " + users);
        	repositorio.save(users);
            return new ResponseEntity(HttpStatus.CREATED);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/all")
    public List<Usuario> allUsers(){
        return repositorio.findAll();
    }

    @GetMapping("/find/{userid}")
    public Usuario findSingleUser(@PathVariable("userid") Long userId){
        return repositorio.findById(userId).orElse(null);
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateUser(@RequestBody Usuario users){
    	repositorio.save(users);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/find/{userid}")
    public ResponseEntity<?> deleteUserById(@PathVariable("userid") Long userId){
    	repositorio.deleteById(userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}